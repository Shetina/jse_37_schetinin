package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.IConnectionService;
import ru.t1.schetinin.tm.api.service.IProjectService;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.TaskNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.ConnectionService;
import ru.t1.schetinin.tm.service.ProjectService;
import ru.t1.schetinin.tm.service.PropertyService;
import ru.t1.schetinin.tm.service.TaskService;
import ru.t1.schetinin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    public final static Project PROJECT_TEST1 = new Project();

    @NotNull
    public final static Task TASK_TEST1 = new Task();

    @NotNull
    public final static Task TASK_TEST2 = new Task();

    @NotNull
    public final static Task TASK_TEST3 = new Task();

    @NotNull
    public final static String TASK_ID_FAKE = UUID.randomUUID().toString();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final Connection CONNECTION = CONNECTION_SERVICE.getConnection();

    @NotNull
    private static final IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_REPOSITORY.add(user);
        USER_ID = user.getId();
        PROJECT_TEST1.setName("Project_task_1");
        PROJECT_TEST1.setDescription("description_1");
        TASK_TEST1.setName("Test_task_1");
        TASK_TEST1.setDescription("description_1");
        TASK_TEST1.setProjectId(PROJECT_TEST1.getId());
        TASK_TEST2.setName("Test_task_2");
        TASK_TEST2.setDescription("description_2");
        TASK_TEST2.setProjectId(PROJECT_TEST1.getId());
        TASK_TEST3.setName("Test_task_3");
        TASK_TEST3.setDescription("description_3");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = USER_REPOSITORY.findByLogin("test_login");
        if (user != null) USER_REPOSITORY.remove(user);
    }

    @Before
    public void initDemoData() throws Exception {
        PROJECT_SERVICE.add(USER_ID, PROJECT_TEST1);
        TASK_SERVICE.add(USER_ID, TASK_TEST1);
        TASK_SERVICE.add(USER_ID, TASK_TEST2);
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void testAddTask() throws Exception {
        Assert.assertNotNull(TASK_SERVICE.add(USER_ID, TASK_TEST3));
        @Nullable final Task task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_TEST3.getId(), task.getId());
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final Task task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertTrue(TASK_SERVICE.existsById(task.getId()));
        Assert.assertTrue(TASK_SERVICE.existsById(task.getUserId(), task.getId()));
        Assert.assertFalse(TASK_SERVICE.existsById(TASK_ID_FAKE));
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final Task task = TASK_SERVICE.create(USER_ID, TASK_TEST3.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
    }

    @Test
    public void testCreateTaskWithDesc() throws Exception {
        @NotNull final Task task = TASK_SERVICE.create(USER_ID, TASK_TEST3.getName(), TASK_TEST3.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
        Assert.assertEquals(TASK_TEST3.getName(), task.getName());
        Assert.assertEquals(TASK_TEST3.getDescription(), task.getDescription());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Task> tasks = TASK_SERVICE.findAll();
        @NotNull final List<Task> tasks2 = TASK_SERVICE.findAll(USER_ID);
        Assert.assertEquals(tasks.size(), TASK_SERVICE.getSize());
        Assert.assertEquals(tasks2.size(), TASK_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testFindAllSort() throws Exception {
        @NotNull final String sortType = "BY_CREATED";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Project> projectSort = TASK_SERVICE.findAll(USER_ID, sort.getComparator());
        Assert.assertNotNull(TASK_SERVICE.findAll(sort.getComparator()));
        Assert.assertEquals(TASK_SERVICE.findAll(USER_ID).toString(), projectSort.toString());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<Task> tasks = TASK_SERVICE.findAll(USER_ID);
        @NotNull final Task task1 = tasks.get(0);
        @NotNull final String taskId = task1.getId();
        Assert.assertEquals(task1.toString(), TASK_SERVICE.findOneById(taskId).toString());
    }

    @Test
    public void testClearUser() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, TASK_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testRemove() throws Exception {
        @Nullable final Task removeTask = TASK_SERVICE.remove(TASK_TEST2);
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_SERVICE.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveUser() throws Exception {
        @Nullable final Task removeTask = TASK_SERVICE.remove(USER_ID, TASK_TEST2);
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_SERVICE.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        @Nullable final Task removeTask = TASK_SERVICE.removeById(TASK_TEST2.getId());
        Assert.assertNotNull(removeTask);
        Assert.assertEquals(TASK_TEST2.getId(), removeTask.getId());
        Assert.assertNull(TASK_SERVICE.findOneById(removeTask.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        @Nullable final Task removeTask = TASK_SERVICE.remove(USER_ID, TASK_TEST2);
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.removeById(USER_ID, null));
        Assert.assertNull(TASK_SERVICE.removeById("USER_ID_FAKE", removeTask.getId()));
    }

    @Test
    public void testRemoveAll() throws Exception {
        @NotNull final List<Task> removeTasks = TASK_SERVICE.findAll(USER_ID);
        TASK_SERVICE.removeAll(removeTasks);
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void testTaskFindAllByProjectId() throws Exception {
        @NotNull final List<Task> tasks = TASK_SERVICE.findAllByProjectId(USER_ID, PROJECT_TEST1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void testCreateNameNull() {
        @NotNull final String userId = USER_ID;
        @Nullable final String taskName = null;
        @NotNull final String desc = "desc";
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateUserIdNull() {
        @Nullable final String userId = null;
        @NotNull final String taskName = "TaskNullId";
        @NotNull final String desc = "desc";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testCreateDescriptionNull() {
        @NotNull final String userId = USER_ID;
        @NotNull final String taskName = "TaskNullId";
        @Nullable final String desc = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(userId, taskName, desc));
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        TASK_SERVICE.updateById(USER_ID, TASK_TEST1.getId(), name, desc);
        @Nullable final Task task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(desc, task.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final Task task = TASK_TEST1;
        @NotNull final String name = "TaskUpdate";
        @NotNull final String desc = "descUpdate";
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, task.getId(), name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(userId, task.getId(), name, null));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(userIdFake, task.getId(), name, desc));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.IN_PROGRESS;
        TASK_SERVICE.changeTaskStatusById(USER_ID, TASK_TEST1.getId(), status);
        @Nullable final Task task = TASK_SERVICE.findOneById(USER_ID, TASK_TEST1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final Task task = TASK_TEST1;
        @NotNull final Status status = Status.IN_PROGRESS;
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, task.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(userIdFake, task.getId(), status));
    }

}
