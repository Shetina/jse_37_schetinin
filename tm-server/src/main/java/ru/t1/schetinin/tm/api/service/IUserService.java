package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @NotNull
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    User removeOne(@Nullable User model) throws Exception;

    void removeByLogin(@Nullable String login) throws Exception;

    void removeByEmail(@Nullable String email) throws Exception;

    void setPassword(@Nullable String id, @Nullable String password) throws Exception;

    void updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    Boolean isLoginExist(@Nullable String login) throws Exception;

    Boolean isEmailExist(@Nullable String email) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}