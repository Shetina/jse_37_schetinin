package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.endpoint.ISystemEndpoint;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}